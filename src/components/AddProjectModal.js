import React, { useState } from 'react'
import { Button, Form, FormControl, InputGroup, Modal } from 'react-bootstrap'

const AddProjectModal = (props) => {
  const [name, setName] = useState('');
  const [desc, setDesc] = useState('');

  const handleProjNameChange = (e) => {
    setName(e.target.value);
    props.addProjTemp(e.target.value);
  }

  const handleProjDescChange = (e) => {
    setDesc(e.target.value);
  }

  // Add a brand spankin' new project.
  const addProject = () => {
    fetch(`/project/create?name=${name}&desc=${desc}&owner=${props.userId}`, {
      method: "POST",
      headers: {'Authorization': 'Bearer ' + props.accessToken}
    })
    .then(res => res.json())
    .then(
      (result) => {
        props.changeSelectedProject(result['id'])
        props.setModalShow(false);
        props.getProjects()
      },
      (error) => {
        alert(error);
      }
    )
  }

  const cancelModal = () => {
    props.cancelProjCreation();
    props.setModalShow(false);
  }

  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
      backdrop="static"
      keyboard={false}
      onHide={cancelModal}
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Add a project
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <InputGroup className="mb-3">
          <FormControl
            placeholder="Enter project name"
            aria-label="Project name"
            aria-describedby="basic-addon2"
            onChange={handleProjNameChange}
          />
        </InputGroup>
        <Form.Group controlId="exampleForm.ControlTextarea1">
          <Form.Control
            as="textarea"
            rows="3"
            placeholder="Enter project description"
            onChange={handleProjDescChange}
          />
        </Form.Group>
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={addProject} variant="primary">Add</Button>{' '}
        <Button onClick={cancelModal} variant="secondary">Close</Button>
      </Modal.Footer>
    </Modal>
  );
}

export default AddProjectModal
