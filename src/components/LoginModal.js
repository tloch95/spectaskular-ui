import React, { useState } from 'react'
import { Button, Form, Modal } from 'react-bootstrap'
// import axios from 'axios';

const LoginModal = (props) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const handleEmailChange = (e) => {
    setEmail(e.target.value);
  };

  const handlePasswordChange = (e) => {
    setPassword(e.target.value);
  };

  const login = (e) => {
    const formData  = new FormData();
    formData.append('email', email);
    formData.append('password', password);

    // `${process.env.REACT_APP_API_BASE_URL}/login`
    fetch("/login", {
      method: "POST",
      credentials: "include",
      body: formData
    })
    .then(res => res.json())
    .then(
      (res) => {
        // Expiry needs to be in local storage so we can see it as it changes in our setInterval in App.js
        localStorage.setItem('exp', res['exp']);
        localStorage.setItem('uid', res['user_id']);
        localStorage.setItem('username', res['username']);
        props.setUserId(res['user_id']);
        props.setAccessToken(res['access_token']);
        props.setUsername(res['username']);
        props.setIsLoggedIn(true);
        props.setLoginModalShow(false);
      },
      (err) => {
        if (err.response && err.response.status && err.response.status === 422) {
          props.setErrorMsg('Incorrect username or password. Please try again.');
          props.setErrorModalShow(true);
        } else {
          props.setErrorMsg('Oops, sorry! Not sure what happened there... Please add an issue at https://gitlab.com/tloch95/spectaskular-api/-/issues/');
          props.setErrorModalShow(true);
        }
      }
    )

    e.preventDefault();
  }

  const demo = (e) => {
    // `${process.env.REACT_APP_API_BASE_URL}/demo`
    fetch("/demo", {
      method: "POST",
      credentials: "include"
    })
    .then(res => res.json())
    .then(
      (res) => {
        // Expiry needs to be in local storage so we can see it as it changes in our setInterval in App.js
        localStorage.setItem('exp', res['exp']);
        localStorage.setItem('uid', res['user_id']);
        localStorage.setItem('username', res['username']);
        props.setUserId(res['user_id']);
        props.setAccessToken(res['access_token']);
        props.setUsername(res['username']);
        props.setIsLoggedIn(true);
        props.setLoginModalShow(false);
      },
      (err) => {
        props.setErrorMsg('Oops, sorry! There may be a problem with our system. Please add an issue at https://gitlab.com/tloch95/spectaskular-api/-/issues/');
        props.setErrorModalShow(true);
      }
    )

    e.preventDefault();
  }

  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
      backdrop="static"
      keyboard={false}
    >
      <Modal.Header>
        <Modal.Title id="contained-modal-title-vcenter">
          Log In
        </Modal.Title>
      </Modal.Header>
      <Form onSubmit={login}>
        <Modal.Body>
          <Form.Group controlId="formBasicEmail">
            <Form.Label>Email address</Form.Label>
            <Form.Control type="email" placeholder="Enter email" required onChange={handleEmailChange}/>
            <Form.Text className="text-muted">
              We'll never share your email with anyone else.
            </Form.Text>
          </Form.Group>

          <Form.Group controlId="formBasicPassword">
            <Form.Label>Password</Form.Label>
            <Form.Control type="password" placeholder="Password" required onChange={handlePasswordChange}/>
          </Form.Group>
          {/* <Form.Group controlId="formBasicCheckbox">
            <Form.Check type="checkbox" label="Keep me logged in" />
          </Form.Group> */}
        </Modal.Body>
        <Modal.Footer>
          <Button
            variant="dark"
            onClick={demo}
          >
            Demo Mode
          </Button>
          <Button
            variant="info"
            onClick={() => {props.setRegisterModalShow(true); props.setLoginModalShow(false)}}
          >
            Register
          </Button>
          <Button
            variant="primary"
            type="submit"
          >
            Log In
          </Button>
        </Modal.Footer>
      </Form>
    </Modal>
  );
}

export default LoginModal
