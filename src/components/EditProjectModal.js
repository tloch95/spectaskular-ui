import React, { useState } from 'react'
import { Button, Form, FormControl, InputGroup, Modal } from 'react-bootstrap'

const EditProjectModal = (props) => {
  const [name, setName] = useState(props.projName);
  const [desc, setDesc] = useState(props.projDesc);

  const handleProjNameChange = (e) => {
    setName(e.target.value);
    props.editProjTemp(props.projId, e.target.value);
  }

  const handleProjDescChange = (e) => {
    setDesc(e.target.value);
  }

  const editProject = () => {
    fetch(`/project/update?id=${props.projId}&name=${name}&desc=${desc}`, {
      method: "POST",
      headers: {'Authorization': 'Bearer ' + props.accessToken}
    })
    .then(res => res.json())
    .then(
      (result) => {
        props.setEditModalShow(false);
        props.getProjects()
      },
      (error) => {
        alert(error);
      }
    )
  }

  const cancelModal = () => {
    props.cancelProjEdit();
    props.setEditModalShow(false);
  }

  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
      backdrop="static"
      keyboard={false}
      onHide={cancelModal}
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Edit project
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <InputGroup className="mb-3">
          <FormControl
            value={name}
            aria-label="Project name"
            aria-describedby="basic-addon2"
            onChange={handleProjNameChange}
          />
        </InputGroup>
        <Form.Group controlId="exampleForm.ControlTextarea1">
          <Form.Control
            value={desc}
            as="textarea"
            rows="3"
            placeholder="Enter project description"
            onChange={handleProjDescChange}
          />
        </Form.Group>
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={editProject} variant="primary">Edit</Button>{' '}
        <Button onClick={cancelModal} variant="secondary">Close</Button>
      </Modal.Footer>
    </Modal>
  );
}

export default EditProjectModal
