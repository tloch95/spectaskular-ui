import React, { useState } from 'react'
import { Button, Form, Modal } from 'react-bootstrap'

const RegisterModal = (props) => {
  const [username, setUsername] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');

  const handleUsernameChange = (e) => {
    setUsername(e.target.value);
  };

  const handleEmailChange = (e) => {
    setEmail(e.target.value);
  };

  const handlePasswordChange = (e) => {
    setPassword(e.target.value);
  };

  const handleConfirmPasswordChange = (e) => {
    setConfirmPassword(e.target.value);
  };

  const register = (e) => {
    // Make sure passwords match
    if (password !== confirmPassword) {
      alert('Passwords do not match!');
      e.preventDefault();
      return;
    }

    const formData  = new FormData();
    formData.append('username', username);
    formData.append('email', email);
    formData.append('password', password);

    fetch("/register", {
      method: "POST",
      body: formData
    })
    .then(res => res.json())
    .then(
      (result) => {
        alert('Account successfully created! You may now log in.');
        props.setUsername(result['username']);
        props.setRegisterModalShow(false);
        props.setLoginModalShow(true);
      },
      (error) => {
        alert(error);
      }
    )
    e.preventDefault();
  }

  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
      backdrop="static"
      keyboard={false}
    >
      <Modal.Header>
        <Modal.Title id="contained-modal-title-vcenter">
          Register
        </Modal.Title>
      </Modal.Header>
      <Form onSubmit={register}>
        <Modal.Body>
        <Form.Group controlId="formBasicUsername">
            <Form.Label>Username</Form.Label>
            <Form.Control type="text" placeholder="Enter username" required onChange={handleUsernameChange}/>
          </Form.Group>
          <Form.Group controlId="formBasicEmail">
            <Form.Label>Email address</Form.Label>
            <Form.Control type="email" placeholder="Enter email" required onChange={handleEmailChange}/>
            <Form.Text className="text-muted">
              We'll never share your email with anyone else.
            </Form.Text>
          </Form.Group>

          <Form.Group controlId="formBasicPassword">
            <Form.Label>Password</Form.Label>
            <Form.Control type="password" placeholder="Password" required onChange={handlePasswordChange}/>
          </Form.Group>
          <Form.Group controlId="formBasicPasswordConfirm">
            <Form.Label>Confirm Password</Form.Label>
            <Form.Control type="password" placeholder="Confirm Password" required onChange={handleConfirmPasswordChange}/>
          </Form.Group>
        </Modal.Body>
        <Modal.Footer>
          <Button
            variant="info"
            onClick={() => {props.setRegisterModalShow(false); props.setLoginModalShow(true)}}
          >Log In</Button>
          <Button
            variant="primary"
            type="submit"
          >Register</Button>
        </Modal.Footer>
      </Form>
    </Modal>
  );
}

export default RegisterModal
