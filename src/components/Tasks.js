import React from 'react'

import AddTask from './AddTask'
import Task from './Task'

import { ChevronCompactDown, ChevronCompactUp } from 'react-bootstrap-icons';

const Tasks = (props) => {
  const incompleteTasks = props.tasks.filter(task => !task.completed).map((task) => (
    <Task
      task={task}
      key={task.id}
      getTasks={props.getTasks}
      hideAllContextMenus={props.hideAllContextMenus}
      numCompletedTasks={props.numCompletedTasks}
      accessToken={props.accessToken}
    />
  ));

  const completedTasks = props.tasks.filter(task => task.completed).map((task) => (
    <Task
      task={task}
      key={task.id}
      getTasks={props.getTasks}
      hideAllContextMenus={props.hideAllContextMenus}
      numCompletedTasks={props.numCompletedTasks}
      accessToken={props.accessToken}
    />
  ));

  const handleCompletedDivClick = () => {
    props.setCompletedTasksSectionExpanded(!props.completedTasksSectionExpanded);
  }

  // Imitating a button hover.
  const handleMouseEnterCompletedDiv = (e) => {
    var taskDiv = document.getElementById('completedDiv');
    taskDiv.style.backgroundColor = '#3b4559';
  }
  const handleMouseLeaveCompletedDiv = () => {
    var taskDiv = document.getElementById('completedDiv');
    taskDiv.style.backgroundColor = '#323a4a';
  }

  // No project selected
  if (!props.selectedProject) {
    return (
      <div className="col-md-12" style={{padding: '1vh 0'}}>
        <h4 style={{color: '#cccccc'}}>No project selected. Select a project to see your tasks!</h4>
      </div>
    )
  // Project selected, contains tasks
  } else if (props.tasks && props.tasks.length > 0) {
    return (
      <div>
        {incompleteTasks}
        <AddTask 
					selectedProject={props.selectedProject}
					getTasks={props.getTasks}
          accessToken={props.accessToken}
				>
				</AddTask>
        <div
          id="completedDiv"
          style={completedDivStyle}
          onClick={handleCompletedDivClick}
          onMouseEnter={handleMouseEnterCompletedDiv}
          onMouseLeave={handleMouseLeaveCompletedDiv}
        >
          Completed Tasks ({props.numCompletedTasks}) {!props.completedTasksSectionExpanded && <ChevronCompactDown/>} {props.completedTasksSectionExpanded && <ChevronCompactUp/>}
        </div>
        {props.completedTasksSectionExpanded && completedTasks}
      </div>
    )
  // Project selected, contains NO tasks
  } else {
    return (
      <div className="col-md-12" style={{padding: '1vh 0'}}>
        <h4 style={{color: '#cccccc'}}>No tasks created. Click the "Add Task" button to get started on your project!</h4>
        <AddTask 
					selectedProject={props.selectedProject}
					getTasks={props.getTasks}
          accessToken={props.accessToken}
				>
				</AddTask>
      </div>
    )
  }
}

const completedDivStyle = {
  textAlign: 'middle',
  color: 'white',
  padding: '1vh 1vw',
  cursor: 'pointer'
}

export default Tasks
