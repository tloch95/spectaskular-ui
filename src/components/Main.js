import React from 'react'

import Tasks from './Tasks'

const Main = (props) => {
	return (
		<div style={mainStyle}>
			<div className='tasks'
				style={{display:'flex',
				flexDirection:'column'}}>
				
				<Tasks
					tasks={props.tasks}
					getTasks={props.getTasks}
					hideAllContextMenus={props.hideAllContextMenus}
					selectedProject={props.selectedProject}
					numCompletedTasks={props.numCompletedTasks}
					completedTasksSectionExpanded={props.completedTasksSectionExpanded}
					setCompletedTasksSectionExpanded={props.setCompletedTasksSectionExpanded}
					accessToken={props.accessToken}
				/>
			</div>
		</div>
	)
}

const mainStyle = {
	width: '100%',
	height: '100vh',
	padding: '2vh 1vw',
	backgroundColor: '#323a4a',
	overflowY: 'auto'
}

export default Main
