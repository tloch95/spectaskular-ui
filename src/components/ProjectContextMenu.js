import React from 'react'

import DeleteProjectModal from './DeleteProjectModal'
import EditProjectModal from './EditProjectModal'

const ProjectContextMenu = (props) => {
  const [deleteModalShow, setDeleteModalShow] = React.useState(false);
  const [editModalShow, setEditModalShow] = React.useState(false);

  return (
    <div
      className='projContextMenu contextMenu'
      id={'projContextMenu-' + props.projId}
    >
      <button
        className="col-md-12"
        onClick={() => setEditModalShow(true)}
      >
        Edit Project
      </button>
      <button 
        className="col-md-12"
        onClick={() => setDeleteModalShow(true)}
      >
        Delete Project
      </button>
      <DeleteProjectModal
        show={deleteModalShow}
        setDeleteModalShow={setDeleteModalShow}
        projName={props.projName}
        projId={props.projId}
        getProjects={props.getProjects}
        changeSelectedProject={props.changeSelectedProject}
        accessToken={props.accessToken}
      ></DeleteProjectModal>
      <EditProjectModal
        show={editModalShow}
        setEditModalShow={setEditModalShow}
        projDesc={props.projDesc}
        projName={props.projName}
        projId={props.projId}
        getProjects={props.getProjects}
        editProjTemp={props.editProjTemp}
        cancelProjEdit={props.cancelProjEdit}
        accessToken={props.accessToken}
      ></EditProjectModal>
    </div>
  )
}

export default ProjectContextMenu
