import React from 'react'

import { Button, Modal } from 'react-bootstrap'

const DeleteProjectModal = (props) => {
  const cancelModal = () => {
    props.setDeleteModalShow(false);
  }

  const deleteProject = () => {
    fetch(`/project/delete?id=${props.projId}`, {
      method: "DELETE",
      headers: {'Authorization': 'Bearer ' + props.accessToken}
    })
    .then(
      (result) => {
        props.changeSelectedProject();
        props.setDeleteModalShow(false);
        props.getProjects()
      },
      (error) => {
        alert(error);
      }
    )
  }
  
  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
      backdrop="static"
      keyboard={false}
      onHide={cancelModal}
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Delete project '{props.projName}'?
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        This action will delete the project as well as all tasks (both completed and incomplete) that are associated with this project PERMANENTLY. This cannot be undone. Are you sure?
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={deleteProject} variant="danger">Delete</Button>{' '}
        <Button onClick={cancelModal} variant="secondary">Cancel</Button>
      </Modal.Footer>
    </Modal>
  );
}

export default DeleteProjectModal

