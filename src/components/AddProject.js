import React from 'react'

import AddIcon from '@material-ui/icons/Add'
import Fab from '@material-ui/core/Fab'
import Tooltip from '@material-ui/core/Tooltip'

import AddProjectModal from './AddProjectModal'

const AddProject = (props) => {
  const [modalShow, setModalShow] = React.useState(false);

  return (
    <div style={{padding: '1vh 0'}}>
      <Tooltip title="Add Project" arrow placement="right">
        <Fab
          color="primary"
          aria-label="add"
          onClick={() => setModalShow(true)}
        >
          <AddIcon />
        </Fab>
      </Tooltip>
      <AddProjectModal
				show={modalShow}
        cancelProjCreation={props.cancelProjCreation}
        projects={props.projects}
        addProjTemp={props.addProjTemp}
        setModalShow={setModalShow}
        getProjects={props.getProjects}
        changeSelectedProject={props.changeSelectedProject}
        userId={props.userId}
        accessToken={props.accessToken}
			/>
    </div>
  );
}

export default AddProject
