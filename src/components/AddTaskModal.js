import React, { useState } from 'react'

import { Button, Form, FormControl, InputGroup, Modal } from 'react-bootstrap'

const AddTaskModal = (props) => {
  const [title, setTitle] = useState('');
  const [desc, setDesc] = useState('');

  const handleTaskTitleChange = (e) => {
    setTitle(e.target.value);
  }

  const handleTaskDescChange = (e) => {
    setDesc(e.target.value);
  }

  // Add a brand spankin' new task.
  const addTask= () => {
    fetch(`/task/create?title=${title}&desc=${desc}&project=${props.selectedProject}`, {
      method: "POST",
      headers: {'Authorization': 'Bearer ' + props.accessToken}
    })
    .then(res => res.json())
    .then(
      (result) => {
        props.setModalShow(false);
        props.getTasks(props.selectedProject)
        setTitle('');
        setDesc('');
      },
      (error) => {
        alert(error);
      }
    )
  }

  const cancelModal = () => {
    props.setModalShow(false);
  }

  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
      backdrop="static"
      keyboard={false}
      onHide={cancelModal}
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Add a task
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <InputGroup className="mb-3">
          <FormControl
            placeholder="Enter task title"
            aria-label="Task title"
            aria-describedby="basic-addon2"
            onChange={handleTaskTitleChange}
          />
        </InputGroup>
        <Form.Group controlId="exampleForm.ControlTextarea1">
          <Form.Control
            as="textarea"
            rows="3"
            placeholder="Enter task description"
            onChange={handleTaskDescChange}
          />
        </Form.Group>
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={addTask} variant="primary">Add</Button>{' '}
        <Button onClick={cancelModal} variant="secondary">Close</Button>
      </Modal.Footer>
    </Modal>
  );
}

export default AddTaskModal
