//import './SideMenu.css'
import axios from 'axios';
import React from 'react';
import { Button } from 'react-bootstrap'

import Projects from './Projects';

const SideMenu = (props) => {
	const logout = (e) => {
		// Invalidate refresh token
		axios({
			method: 'post',
			url: "/logout/refresh",
			credentials: "include"
		})

		// Invalidate our access token
		axios({
			method: 'post',
			url: "/logout/access",
			headers: {'Authorization': 'Bearer ' + props.accessToken}
		})
		.then(res => {
			props.setAccessToken(null);
			props.setUsername(null);
			props.setIsLoggedIn(false);
			props.setLoginModalShow(true);
			props.setUserId(null);
			props.setProjects([]);
			props.setTasks([]);
			props.setSelectedProject(null);
		})

		e.preventDefault();
	}

	return (
		<div style={sideMenuStyle}>
			<h2 style={spectaskularStyle}><u style={{textDecorationColor: '#a2b9de'}}>Spectaskular</u></h2>
			{
				<Projects
					projects={props.projects}
					changeSelectedProject={props.changeSelectedProject}
					getProjects={props.getProjects}
					selectedProject={props.selectedProject}
					hideAllContextMenus={props.hideAllContextMenus}
					editProjTemp={props.editProjTemp}
					cancelProjEdit={props.cancelProjEdit}
					addProjTemp={props.addProjTemp}
					cancelProjCreation={props.cancelProjCreation}
					userId={props.userId}
					accessToken={props.accessToken}
				/>
			}
			{
				props.isLoggedIn &&
				<Button
					onClick={logout}
					style={{position: 'fixed', bottom: '15px', left: '15px'}}
				>Logout</Button>
			}
		</div>
	);
}

const sideMenuStyle = {
	backgroundColor: '#282c34',
	height: '100vh',
	padding: '2vh 1vw',
	borderRight: 'solid #23272e 2px',
	display: 'flex',
	flexDirection: 'column',
	width: '30vw',
	overflowY: 'auto'
}

const spectaskularStyle = {
	color: 'white'
}

export default SideMenu;
