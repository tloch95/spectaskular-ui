import React from 'react'
import { Button, Modal } from 'react-bootstrap'

const ErrorModal = (props) => {
  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
      backdrop="static"
      keyboard={false}
      style={{zIndex: '10000000000'}}
    >
      <Modal.Header>
        <Modal.Title id="contained-modal-title-vcenter">
          Error
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {props.errorMsg && <p style={{color: 'red'}}>{props.errorMsg}</p>}
      </Modal.Body>
      <Modal.Footer>
        <Button
          variant="primary"
          onClick={() => {props.setErrorModalShow(false)}}
        >Ok</Button>
      </Modal.Footer>
    </Modal>
  );
}

export default ErrorModal
