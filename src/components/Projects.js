import React from 'react'

import AddProject from './AddProject'
import Project from './Project';

const Projects = (props) => {
  const projects = props.projects && props.projects.length != 0 && Array.isArray(props.projects) ? props.projects.map((project) => (
    <Project project={project}
        key={project.id}
        changeSelectedProject={props.changeSelectedProject}
        getProjects={props.getProjects}
        selectedProject={props.selectedProject}
        hideAllContextMenus={props.hideAllContextMenus}
        editProjTemp={props.editProjTemp}
        cancelProjEdit={props.cancelProjEdit}
        accessToken={props.accessToken}
      />
  )) : <h5 style={{color: '#CCCCCC'}}>No projects yet! Add one to start your path to productivity!</h5>;

  return (
    <div>
      {projects}
      <AddProject
          projects={props.projects}
          addProjTemp={props.addProjTemp}
          cancelProjCreation={props.cancelProjCreation}
          getProjects={props.getProjects}
          changeSelectedProject={props.changeSelectedProject}
          userId={props.userId}
          accessToken={props.accessToken}
      />
    </div>
  )
}

export default Projects
