import React from 'react'

import DeleteTaskModal from './DeleteTaskModal'
import EditTaskModal from './EditTaskModal'

const TaskContextMenu = (props) => {
  const [deleteModalShow, setDeleteModalShow] = React.useState(false);
  const [editModalShow, setEditModalShow] = React.useState(false);

  return (
    <div 
      className='taskContextMenu contextMenu'
      id={'taskContextMenu-' + props.taskId}>

      <button
        className="col-md-12"
        onClick={() => setEditModalShow(true)}
      >
        Edit Task
      </button>
      <button 
        className="col-md-12"
        onClick={() => setDeleteModalShow(true)}
      >
        Delete Task
      </button>

      <DeleteTaskModal
        show={deleteModalShow}
        setDeleteModalShow={setDeleteModalShow}
        taskId={props.taskId}
        getTasks={props.getTasks}
        accessToken={props.accessToken}
      ></DeleteTaskModal>
      <EditTaskModal
        show={editModalShow}
        setEditModalShow={setEditModalShow}
        taskId={props.taskId}
        getTasks={props.getTasks}
        title={props.title}
        desc={props.desc}
        completed={props.completed}
        accessToken={props.accessToken}
      ></EditTaskModal>
    </div>
  )
}

export default TaskContextMenu
