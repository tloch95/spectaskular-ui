import React from 'react'
import Tooltip from '@material-ui/core/Tooltip'

import ProjectContextMenu from './ProjectContextMenu'

const Project = (props) => {
  const { id, name } = props.project;
  const description = props.project.description ? props.project.description : '';

  const handleRightClick = (e) => {
    e.preventDefault();
    // Hide any context menus that are already showing.
    props.hideAllContextMenus();

    // Set positioning of context menu to mouse position.
    var contextMenu = document.getElementById(`projContextMenu-${id}`);
    contextMenu.style.position = 'fixed';
    contextMenu.style.left = e.pageX + 'px';
    contextMenu.style.top = e.pageY + 'px';
    contextMenu.style.display = 'initial';

    // In case context menu goes out of viewport
    var viewportHeight = document.documentElement.clientHeight;
    var viewportWidth = document.documentElement.clientWidth;
    if ((e.pageX + contextMenu.clientWidth) > viewportWidth) {
      contextMenu.style.left = `${e.pageX - contextMenu.clientWidth}px`;
    }

    if ((e.pageY + contextMenu.clientHeight) > viewportHeight) {
      contextMenu.style.top = `${e.pageY - contextMenu.clientHeight}px`;
    }
  }

  // Imitating a button hover.
  const handleMouseEnter = (e) => {
    var projBtn = document.getElementById('projBtn-' + id);
    projBtn.style.border = '1px solid #80868c';
  }
  const handleMouseLeave = () => {
    var projBtn = document.getElementById('projBtn-' + id);
    projBtn.style.border = '1px solid #282c34';
  }

  let style = btnStyle;
  if (props.selectedProject === id) {
    style = selectedBtnStyle;
  }

  return (
    <div>
      <Tooltip title={description} arrow placement="right" interactive>
        <button
          id={'projBtn-' + id}
          className="projBtn"
          style={style}
          onClick={()=>props.changeSelectedProject(id)}
          onContextMenu={handleRightClick}
          onMouseEnter={handleMouseEnter}
          onMouseLeave={handleMouseLeave}
        >
          <h4 style={pStyle}>
            {name}
          </h4>
        </button>
      </Tooltip>
      <ProjectContextMenu
        projId={id}
        projName={name}
        projDesc={description}
        getProjects={props.getProjects}
        changeSelectedProject={props.changeSelectedProject}
        editProjTemp={props.editProjTemp}
        cancelProjEdit={props.cancelProjEdit}
        accessToken={props.accessToken}
      >
      </ProjectContextMenu>
    </div>
  )
}

const btnStyle = {
	border: '1px solid #282c34',
	backgroundColor: 'transparent',
	color: 'white',
  cursor: 'pointer',
  outline: 'none'
}

const selectedBtnStyle = {
	border: '1px solid #282c34',
	backgroundColor: '#363d4a',
	color: 'white',
  cursor: 'pointer',
  outline: 'none'
}

const pStyle = {
  padding: '1vh 0.25vw',
  margin: '0px',
  minWidth: '20vw',
	maxWidth: '30vw',
}

export default Project
