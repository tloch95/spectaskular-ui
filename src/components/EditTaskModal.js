import React, { useState } from 'react'
import { Button, Form, FormControl, InputGroup, Modal } from 'react-bootstrap'

const EditTaskModal = (props) => {
  const [title, setTitle] = useState(props.title);
  const [desc, setDesc] = useState(props.desc);

  const handleTaskTitleChange = (e) => {
    setTitle(e.target.value);
  }

  const handleTaskDescChange = (e) => {
    setDesc(e.target.value);
  }

  const editTask = () => {
    fetch(`/task/update?id=${props.taskId}&title=${title}&desc=${desc}&completed=${props.completed}`, {
      method: "POST",
      headers: {'Authorization': 'Bearer ' + props.accessToken}
    })
    .then(res => res.json())
    .then(
      (result) => {
        props.setEditModalShow(false);
        props.getTasks()
      },
      (error) => {
        alert(error);
      }
    )
  }

  const cancelModal = () => {
    props.setEditModalShow(false);
  }

  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
      backdrop="static"
      keyboard={false}
      onHide={cancelModal}
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Edit task
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <InputGroup className="mb-3">
          <FormControl
            value={title}
            aria-label="Task title"
            aria-describedby="basic-addon2"
            onChange={handleTaskTitleChange}
          />
        </InputGroup>
        <Form.Group controlId="exampleForm.ControlTextarea1">
          <Form.Control
            value={desc}
            as="textarea"
            rows="3"
            placeholder="Enter task description"
            onChange={handleTaskDescChange}
          />
        </Form.Group>
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={editTask} variant="primary">Edit</Button>{' '}
        <Button onClick={cancelModal} variant="secondary">Close</Button>
      </Modal.Footer>
    </Modal>
  );
}

export default EditTaskModal
