import React from 'react'

import AddIcon from '@material-ui/icons/Add'
import Fab from '@material-ui/core/Fab'
import Tooltip from '@material-ui/core/Tooltip'

import AddTaskModal from './AddTaskModal'

const AddTask = (props) => {
  const [modalShow, setModalShow] = React.useState(false);

  if (props.selectedProject) {
    return (
      <div style={{padding: '1vh 0'}}>
        <Tooltip title="Add Task" arrow placement="right">
          <Fab
            color="primary"
            aria-label="add"
            onClick={() => setModalShow(true)}
          >
            <AddIcon />
          </Fab>
        </Tooltip>
        <AddTaskModal
          show={modalShow}
          setModalShow={setModalShow}
          selectedProject={props.selectedProject}
          getTasks={props.getTasks}
          accessToken={props.accessToken}
        />
      </div>
    );
  } else {
    return (
      <div>
      </div>
    );
  }
}

export default AddTask
