import React from 'react'

import { Button, Modal } from 'react-bootstrap'

const DeleteTaskModal = (props) => {
  const cancelModal = () => {
    props.setDeleteModalShow(false);
  }

  const deleteTask = () => {
    fetch(`/task/delete?id=${props.taskId}`, {
      method: "DELETE",
      headers: {'Authorization': 'Bearer ' + props.accessToken}
    })
    .then(
      (result) => {
        props.setDeleteModalShow(false);
        props.getTasks()
      },
      (error) => {
        alert(error);
      }
    )
  }
  
  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
      backdrop="static"
      keyboard={false}
      onHide={cancelModal}
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Delete task?
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        This action will delete the task PERMANENTLY. This cannot be undone. Are you sure?
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={deleteTask} variant="danger">Delete</Button>{' '}
        <Button onClick={cancelModal} variant="secondary">Cancel</Button>
      </Modal.Footer>
    </Modal>
  );
}

export default DeleteTaskModal

