import React, { useEffect, useState } from 'react'

import TaskContextMenu from './TaskContextMenu'

const Task = (props) => {
  const { description, id, title } = props.task;
  const [completed, setCompleted] = useState(props.task.completed);
  const [mousePosition, setMousePosition] = useState({ x: null, y: null });

  const updateMousePosition = e => {
    setMousePosition({ x: e.clientX, y: e.clientY });
  };

  useEffect(() => {
    window.addEventListener("mousemove", updateMousePosition);

    return () => window.removeEventListener("mousemove", updateMousePosition);
  }, []);

  const handleRightClick = (e) => {
    e.preventDefault();
    // Hide any context menus that are already showing.
    props.hideAllContextMenus();

    // Set positioning of context menu to mouse position.
    var contextMenu = document.getElementById(`taskContextMenu-${id}`);
    contextMenu.style.position = 'fixed';
    contextMenu.style.left = `${e.pageX}px`;
    contextMenu.style.top = `${e.pageY}px`;
    contextMenu.style.display = 'initial';
    
    // In case context menu goes out of viewport
    var viewportHeight = document.documentElement.clientHeight;
    var viewportWidth = document.documentElement.clientWidth;
    if ((e.pageX + contextMenu.clientWidth) > viewportWidth) {
      contextMenu.style.left = `${e.pageX - contextMenu.clientWidth}px`;
    }

    if ((e.pageY + contextMenu.clientHeight) > viewportHeight) {
      contextMenu.style.top = `${e.pageY - contextMenu.clientHeight}px`;
    }
  }

  // Imitating a button hover.
  const handleMouseEnter = (e) => {
    var taskDiv = document.getElementById('taskDiv-' + id);
    taskDiv.style.backgroundColor = '#3b4559';
  }
  const handleMouseLeave = () => {
    var taskDiv = document.getElementById('taskDiv-' + id);
    taskDiv.style.backgroundColor = '#323a4a';
  }

  const handleCompletedCheckboxChange = (e) => {
    // I am putting way too much work into this trivial click effect
    var colors = ['#ff0000', '#ffa500', '#ffff00', '008000', '#0000ff', '#4b0082', '#ee82ee'];
    if (!completed) {
      var spectaskular = document.createElement('span');
      spectaskular.innerHTML = 'Spectaskular!';
      spectaskular.style.position = 'fixed';
      spectaskular.style.left = (mousePosition['x'] + 10) + 'px';
      spectaskular.style.top = (mousePosition['y'] + 10) + 'px';
      spectaskular.style.pointerEvents = 'none';
      spectaskular.setAttribute("id", "spectaskular");
      document.body.appendChild(spectaskular);

      // Iterates 50 times
      var pos = (mousePosition['y'] + 10);
      var translateEffect = setInterval(function () {
        if (pos === (mousePosition['y'] - 40)) {
          spectaskular = document.getElementById('spectaskular');
          spectaskular.parentNode.removeChild(spectaskular);
          clearInterval(translateEffect);
        } else {
          pos--; 
          spectaskular.style.top = pos + 'px';
        }
      }, 50);
      
      // Iterates 21 times
      var colorIdx = 0;
      var cycleCount = 1;
      var cycleMax = 3;
      var rainbowEffect = setInterval(function () {
        spectaskular.style.color = colors[colorIdx];
        colorIdx++;
        if (colorIdx === (colors.length + 1)) {
          if (cycleCount === cycleMax) {
            clearInterval(rainbowEffect);
          } else {
            cycleCount++;
            colorIdx = 0;
          }
        }
      }, 100);

      // Iterates 10 times
      var fadeEffect = setInterval(function () {
        if (!spectaskular.style.opacity) {
          spectaskular.style.opacity = 1;
        }
        if (spectaskular.style.opacity > 0) {
          spectaskular.style.opacity -= 0.1;
        } else {
            clearInterval(fadeEffect);
        }
      }, 200);
    }

    // Request to "complete" or "uncomplete?" task
    fetch(`/task/complete?id=${id}&completed=${!completed}`, {
      method: "POST",
      headers: {'Authorization': 'Bearer ' + props.accessToken}
    })
    .then(res => res.json())
    .then(
      (result) => {
        setCompleted(!completed);
        props.getTasks();
      },
      (error) => {
        alert(error);
      }
    )
  }

  return (
    <div
      style={taskStyle}
      id={'taskDiv-' + id}
      className="taskDiv col-md-12"
      onContextMenu={handleRightClick}
      onMouseEnter={handleMouseEnter}
      onMouseLeave={handleMouseLeave}
    >
      <div className="col-md-1" style={checkboxDivStyle}>
        <input type="checkbox"
          defaultChecked={completed}
          id={'task-' + id + '-checkbox'}
          name={'task-' + id + '-checkbox'}
          value='Completed'
          onChange={handleCompletedCheckboxChange}
          style={{cursor: 'pointer'}}
        />
        <label htmlFor={'task-' + id + '-checkbox'}></label>
      </div>
      <div className="col-md-11">
        {
          title &&
          <h3 style={titleStyle}>
            {title}
          </h3>
        }
        {
          description &&
          <p style={descStyle}>
            {description}
          </p>
        }
      </div>
      <TaskContextMenu
        taskId={id}
        getTasks={props.getTasks}
        title={title}
        desc={description}
        completed={completed}
        accessToken={props.accessToken}
      >
      </TaskContextMenu>
    </div>
  )
}

const taskStyle = {
  color:'white',
  textAlign:'left',
  borderBottom:'2px dashed #23272e',
  padding: '0 15px',
  display: 'flex',
  height: '15vh'
}

const titleStyle = {
  width:'100%',
  margin:'1vh 0'
}

const descStyle = {
  width:'100%',
  margin:'1vh 0'
}

const checkboxDivStyle = {
  height: '100%',
  textAlign: 'center',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  width: '10vw'
}

export default Task
