import React, { useEffect, useState } from 'react';

import './App.css';
import ErrorModal from './components/ErrorModal'
import LoginModal from './components/LoginModal'
import Main from './components/Main'
import RegisterModal from './components/RegisterModal'
import SideMenu from './components/SideMenu'

import 'bootstrap/dist/css/bootstrap.min.css';

const App = () => {
  const [errorMsg, setErrorMsg] = useState('');
  const [projects, setProjects] = useState([]);
  const [selectedProject, setSelectedProject] = useState();
  const [tasks, setTasks] = useState([]);
  const [numCompletedTasks, setNumCompletedTasks] = useState(tasks.filter(tmpTask => tmpTask.completed).length);
  const [completedTasksSectionExpanded, setCompletedTasksSectionExpanded] = useState(false);
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const [errorModalShow, setErrorModalShow] = useState(false);
  const [loginModalShow, setLoginModalShow] = useState(true);
  const [registerModalShow, setRegisterModalShow] = useState(false);
  const [username, setUsername] = useState('');
  const [userId, setUserId] = useState(null);
  const [accessToken, setAccessToken] = useState(null);
  const [refreshInterval, setRefreshInterval] = useState(null);

  useEffect(() => {
    document.body.addEventListener('click', handleBodyClick);

    // If user is not logged in, we should try and refresh the access token.
    // If the refresh fails, user should stay logged out, and get prompted with login modal.
    if (!isLoggedIn) {
      fetch("/refresh", {
        method: "POST",
        credentials: "include"
      })
      .then((res) => {
        if (res.ok) {
          return res.json();
        } else if (res.status != 401) {
          throw new Error("Other");
        } else {
          // This is a failed refresh (401). We just want to keep the user logged out (forcing re-log in) in this case.
          throw new Error("Unauthorized");
        }
      })
      .then((data) => {
        const accToken = data['access_token'];
        localStorage.setItem('exp', data['exp']);
        setUserId(localStorage.getItem('uid'));
        setAccessToken(accToken);
        setIsLoggedIn(true);
      })
      .catch((err) => {
        if (err != "Error: Unauthorized") {
          setErrorMsg("Sorry! We've encountered a system issue. We recommend that you clear your cookies and try refreshing the page.");
          setErrorModalShow(true);
        }
      });
    }
  }, []);

  useEffect(() => {
    if (accessToken && userId) {
      setIsLoggedIn(true);
      getProjects();
      const refreshIntervalSeconds = 10;

      // Set our refresh token interval, if it isn't set yet
      if (!refreshInterval) {
        const interval = setInterval(() => {
          refreshIfNeeded();
        }, refreshIntervalSeconds * 1000);
      setRefreshInterval(interval);
      }
    } else {
      // Clear our refresh token interval, don't need to refresh if we don't even have an access token
      clearInterval(refreshInterval);
    }
  }, [accessToken]);

  // Function to check if our access token needs refreshing, and to do the refresh as well.
  const refreshIfNeeded = () => {
    var timeNow = new Date();

    // Convert to UTC
    const millisecondsInHour = 60000;
    var utcTimeNow = timeNow.getTime() + (timeNow.getTimezoneOffset() * millisecondsInHour);


    // If now is greater than the expiry time minus 30 seconds
    // In other words - if access token is within 30 seconds of expiring
    const refreshBufferSeconds = 30;
    var exp = new Date(localStorage.getItem('exp'));
    if (new Date(utcTimeNow) >= new Date(exp.getTime() - refreshBufferSeconds * 1000)) {
      // Refresh the token!
      // `${process.env.REACT_APP_API_BASE_URL}/refresh`
      fetch("/refresh", {
        method: "POST",
        credentials: "include"
      })
      .then(res => res.json())
      .then(
        (res) => {
          // If we failed, user needs to log back in
          if (res.status == 401) {
            setIsLoggedIn(false);
            return;
          }

          const accToken = res['access_token'];
          localStorage.setItem('exp', res['exp']);
          setAccessToken(accToken);
        },
        (error) => {
          alert(error);
        }
      )
    }
  }

  const handleBodyClick = (e) => {
    // Hide any project context menus that are already showing.
    hideAllContextMenus();
  }

  // Get the existing projects for this user.
  const getProjects = (uid) => {
    var id = userId ? userId : uid
    fetch(`/projects/read?owner_id=${id}`, {
        method: "GET",
        headers: {'Authorization': 'Bearer ' + accessToken}
      })
    .then(res => res.json())
    .then(
      (result) => {
        setProjects(result);
      },
      (error) => {
        alert(error);
      }
    )
  }

  // Get the existing tasks for this project.
  const getTasks = (project_id) => {
    if (!project_id) {
      project_id = selectedProject;
    }

    fetch(`/tasks/read?project_id=${project_id}`, {
      method: "GET",
      headers: {'Authorization': 'Bearer ' + accessToken}
    })
    .then((res => res.json()))
    .then(
      (result) => {
        setTasks(result);
        setNumCompletedTasks(result.filter(tmpTask => tmpTask.completed).length);
      },
      (error) => {
        alert(error);
      }
    )
  }

  // Adds a temporary project to the list of projects
  // This is purely an "extra" feature, just to add a
  // visual of the project while the user types the 
  // new project's name.
  const addProjTemp = (name) => {
    var existingTempProjIdx = null;

    // Get index for existing temp project if it exists
    projects.forEach((proj, idx) => {
      if (proj['id'] === -1) {
        existingTempProjIdx = idx;
      }
    });

    // Add the temp project if it does not exist
    if (existingTempProjIdx == null) {
      var newProjs = projects.concat({id: -1, name: name, desc: ''});
      setProjects(newProjs);
    // Edit the existing temp project
    } else {
      let newProjs = [...projects];
      let newProj = {...projects[existingTempProjIdx]};
      newProj.name = name;
      newProjs[existingTempProjIdx] = newProj;
      setProjects(newProjs);
    }
  }

  // Edits a project's name temporarily, for a visual representation
  // of the project while the user types a project's new name in the
  // EditProjectModal.
  const editProjTemp = (id, name) => {
    var projIdx = null;

    // Get index for project
    projects.forEach((proj, idx) => {
      if (proj['id'] === id) {
        projIdx = idx;
      }
    });

    let tmpProjs = [...projects];
    let tmpProj = {...projects[projIdx]};
    tmpProj.name = name;
    tmpProjs[projIdx] = tmpProj;
    setProjects(tmpProjs);
  }

  // Triggered if the editing of a project
  // is cancelled, thus reverting the "temporary"
  // project back to its former name.
  const cancelProjEdit = () => {
    getProjects();
  }

  // Change the "selected" project to the newly clicked one.
  const changeSelectedProject = (id) => {
    // Resetting selected proj if none selected.
    if (!id) {
      setSelectedProject();
      setTasks([]);
      return;
    }
    setSelectedProject(id)
    setCompletedTasksSectionExpanded(false);
    getTasks(id);
  }

  // Triggered if the creation of a new project
  // is cancelled, thus deleting the "temporary"
  // project.
  const cancelProjCreation = () => {
    setProjects(projects.filter(proj => proj.id !== -1));
  }

  const hideAllContextMenus = () => {
    var contextMenus = document.getElementsByClassName('contextMenu');
    for (var i = 0; i < contextMenus.length; i++) {
      contextMenus.item(i).style.display = 'none';
    }
  }
  return (
    <div className="App">
        <SideMenu 
          projects={projects}
          changeSelectedProject={changeSelectedProject}
          addProjTemp={addProjTemp}
          cancelProjCreation={cancelProjCreation}
          cancelProjEdit={cancelProjEdit}
          getProjects={getProjects}
          selectedProject={selectedProject}
          hideAllContextMenus={hideAllContextMenus}
          editProjTemp={editProjTemp}
          isLoggedIn={isLoggedIn}
          setIsLoggedIn={setIsLoggedIn}
          setLoginModalShow={setLoginModalShow}
          setUserId={setUserId}
          setErrorMsg={setErrorMsg}
          setErrorModalShow={setErrorModalShow}
          setProjects={setProjects}
          setTasks={setTasks}
          setSelectedProject={setSelectedProject}
          userId={userId}
          accessToken={accessToken}
          setAccessToken={setAccessToken}
          setUsername={setUsername}
        />
        <Main
          hideAllContextMenus={hideAllContextMenus}
          tasks={tasks}
          selectedProject={selectedProject}
          getTasks={getTasks}
          numCompletedTasks={numCompletedTasks}
          completedTasksSectionExpanded={completedTasksSectionExpanded}
          setCompletedTasksSectionExpanded={setCompletedTasksSectionExpanded}
          setErrorMsg={setErrorMsg}
          setErrorModalShow={setErrorModalShow}
          accessToken={accessToken}
        />
      {
        !isLoggedIn &&
        <LoginModal 
          show={loginModalShow}
          setLoginModalShow={setLoginModalShow}
          setRegisterModalShow={setRegisterModalShow}
          setUsername={setUsername}
          setIsLoggedIn={setIsLoggedIn}
          getProjects={getProjects}
          setUserId={setUserId}
          setErrorMsg={setErrorMsg}
          setErrorModalShow={setErrorModalShow}
          setAccessToken={setAccessToken}
        ></LoginModal>
      }
      {
        !isLoggedIn &&
        <RegisterModal 
          show={registerModalShow}
          setLoginModalShow={setLoginModalShow}
          setRegisterModalShow={setRegisterModalShow}
          setUsername={setUsername}
          setErrorMsg={setErrorMsg}
          setErrorModalShow={setErrorModalShow}
        ></RegisterModal>
      }
      <ErrorModal
        show={errorModalShow}
        errorMsg={errorMsg}
        setErrorModalShow={setErrorModalShow}
      ></ErrorModal>
    </div>
  );
}
export default App;